using System.Collections;
using UnityEngine;

namespace Platformer
{
    public class PlayerController : MonoBehaviour
    {
        public float movingSpeed;
        public float jumpForce;
        public float wallClimbSpeed;
        private float moveInput;
        public int currentState;

        private bool facingRight = false;
        [HideInInspector]
        public bool deathState = false;

        public bool isGrounded;
        public bool isTouchingWall;
        public bool isClimbing;
        public Transform groundCheck;
        public Transform wallCheck;
        public bool bloqueado = false;
        private Arbol arbolMasCercano;

        private Rigidbody2D rigidbody;
        private Animator animator;
        public Canvas canvasJugador;
        public Transform transfUsuario;
        private GameManager gameManager;

        private float timeSinceLeftWall; // Tiempo transcurrido desde que se dej� de tocar la pared
        private float valorTiempoSaltoPared = 0.5f;
        private bool haSaltado = false;

        private bool reproduciendoCaminar = false;
        private bool reproduciendoEscalar = false;

        //private readonly string[] tagsExcluidos = { "Arbol" }; //Estos tags son de colliders NO escalables

        void Start()
        {
            rigidbody = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            transfUsuario = GetComponent<Transform>();
            gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        }

        private void FixedUpdate()
        {
            CheckGround();
            CheckWall();
        }

        void Update()
        {
            currentState = animator.GetInteger("playerState");

            HandleMovement();
            HandleJump();
            HandleAttack();
            HandleFlip();
            HandleClimbing();
            SumarTroncosPorTeclado();
        }
        private void SumarTroncosPorTeclado()
        {
            if (Input.GetKeyDown(KeyCode.E) && arbolMasCercano != null)
            {
                arbolMasCercano.SumarTroncosDeArbol();
            }
        }

        private void HandleMovement()
        {
            if (Input.GetButton("Horizontal") && !bloqueado)
            {
                moveInput = Input.GetAxis("Horizontal");
                Vector3 direction = transform.right * moveInput;

                if (isGrounded && !isTouchingWall)
                {
                    isClimbing = false;
                    animator.SetBool("cortarSaltar", false);
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, movingSpeed * Time.deltaTime);
                    animator.SetInteger("playerState", 1); // Turn on run animation

                    //SONIDO
                    if (reproduciendoCaminar == false)
                    {
                        AkSoundEngine.PostEvent("Caminar", gameObject);
                        reproduciendoCaminar = true;
                    }
                    //CualCaminar(true);
                }
                else if (!isGrounded && !isTouchingWall) // Esta volando
                {
                    AkSoundEngine.PostEvent("PararCaminar", gameObject);
                    reproduciendoCaminar = false;
                    //GestorDeAudio.instancia.PausarSonido("CaminarUno");
                    animator.SetInteger("playerState", 2); // Turn on jump animation
                    transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, movingSpeed * Time.deltaTime);
                }
            }
            else if (!Input.GetButton("Horizontal"))
            {
                if (reproduciendoCaminar == true) 
                {
                    AkSoundEngine.PostEvent("PararCaminar", gameObject);
                    reproduciendoCaminar = false;
                }
                //CualCaminar(false);

                if (isGrounded) animator.SetInteger("playerState", 0); // Turn on idle animation
            }
        }
        //private void CualCaminar(bool prender)
        //{
        //    int variableRandom = Random.Range(0, 2); // Genera un n�mero aleatorio entre 0 y 1
        //    if (prender == true)
        //    {
        //        if (reproduciendoCaminar == false)
        //        {
        //            reproduciendoCaminar = true;
        //            if (variableRandom == 0)
        //            {
        //                GestorDeAudio.instancia.LoopearSonido(true, "CaminarUno");
        //                GestorDeAudio.instancia.ReproducirSonido("CaminarUno");
        //            }
        //            else if (variableRandom == 1)
        //            {
        //                GestorDeAudio.instancia.LoopearSonido(true, "CaminarDos");
        //                GestorDeAudio.instancia.ReproducirSonido("CaminarDos");
        //            }
        //        }
        //    }
        //    if (prender == false)
        //    {
        //        GestorDeAudio.instancia.LoopearSonido(false, "CaminarUno");
        //        GestorDeAudio.instancia.LoopearSonido(false, "CaminarDos");
        //        if (reproduciendoCaminar == true)
        //        {
        //            reproduciendoCaminar = false;
        //        }
        //    }
        //}


        private void HandleJump()
        {
            if (Input.GetKeyDown(KeyCode.Space)  && !bloqueado)
            {
                if (isGrounded)
                {
                    // Si est� en el suelo, realizar un salto normal
                    animator.SetInteger("playerState", 2); // Cambiar a animaci�n de salto
                    animator.SetBool("cortarSaltar", true);
                    Invoke("FuerzaSalto", 0.10f);
                }
                else if (!isClimbing && !isTouchingWall && timeSinceLeftWall < valorTiempoSaltoPared && !haSaltado)
                {
                    // Si no est� escalando, no est� tocando la pared y ha pasado menos de cierto tiempo desde que dej� la pared y no ha saltado
                    haSaltado = true; // Marcar que ha realizado el salto
                    animator.SetBool("cortarEscalar", false);
                    //GestorDeAudio.instancia.LoopearSonido(false, "Escalada");
                    AkSoundEngine.PostEvent("PararEscalada", gameObject);
                    rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
                    animator.SetInteger("playerState", 2); // Cambiar a animaci�n de salto
                }
            }

            if (!isGrounded && !isClimbing)
            {
                animator.SetInteger("playerState", 2); // Turn on jump animation
                //GestorDeAudio.instancia.LoopearSonido(false, "Escalada");
                AkSoundEngine.PostEvent("PararEscalada", gameObject);
                animator.SetBool("cortarEscalar", false);
            }
        }

        private void FuerzaSalto()
        {
            rigidbody.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }

        private void HandleAttack()
        {
            if (Input.GetKeyDown(KeyCode.Mouse0) && isGrounded && !bloqueado)
            {
                //GestorDeAudio.instancia.ReproducirSonido("HachazoAire");
                if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Atacando"))
                {
                    AkSoundEngine.PostEvent("Hachazo", gameObject);
                }
                if (currentState == 4)
                {
                    animator.Play("Atacando", -1, 0f); // Reiniciar la animaci�n
                }
                else
                {
                    animator.SetInteger("playerState", 4); // Cambiar al estado de ataque
                }

                // Aseg�rate de que se llama a ChequeoGolpes despu�s de un peque�o retraso para permitir que la animaci�n comience
                if (arbolMasCercano != null)
                {                  
                    Invoke("CallChequeoGolpes", 0.5f); // Ajusta este tiempo seg�n la duraci�n de tu animaci�n
                }
            }
        }

        private void CallChequeoGolpes()
        {
            // Verificar nuevamente que la animaci�n de ataque est� en progreso antes de llamar a ChequeoGolpes
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Atacando") && arbolMasCercano != null)
            {
                arbolMasCercano.ChequeoGolpes();
            }
        }

        private void HandleFlip()
        {
            if (facingRight == false && moveInput > 0 && !bloqueado)
            {
                Flip();
            }
            else if (facingRight == true && moveInput < 0 && !bloqueado)
            {
                Flip();
            }
        }

        private void HandleClimbing()
        {
            if (isClimbing)
            {
               animator.SetInteger("playerState", 5); // Turn on climb animation
               Invoke("CortarEscalada", 0.1f);
               //GestorDeAudio.instancia.LoopearSonido(true, "Escalada");
               if (reproduciendoEscalar == false)
               {
                    AkSoundEngine.PostEvent("Escalada", gameObject);
                    //GestorDeAudio.instancia.ReproducirSonido("Escalada");
                    reproduciendoEscalar = true;
               }
            }
            else
            {
                reproduciendoEscalar = false;
                //GestorDeAudio.instancia.LoopearSonido(false, "Escalada");
                AkSoundEngine.PostEvent("PararEscalada", gameObject);
                animator.SetBool("cortarEscalar", false); // Turn on climb animation            
            }

            if (isTouchingWall && Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f)
            {
                isClimbing = true;
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, Input.GetAxis("Vertical") * wallClimbSpeed);

                timeSinceLeftWall = 0f; // Reiniciar el contador de tiempo
                haSaltado = false; // Reiniciar la variable de salto
            }
            else if (Mathf.Abs(Input.GetAxis("Vertical")) < 0.1f)
            {
                isClimbing = false;
            }
            else if (!isTouchingWall)
            {
                isClimbing = false;
                timeSinceLeftWall += Time.deltaTime; // Incrementar el contador de tiempo
            }
        }

        private void CortarEscalada()
        {
            animator.SetBool("cortarEscalar", true); // Turn on climb animation            
        }

        private void Flip()
        {
            facingRight = !facingRight;

            Vector3 playerScale = transform.localScale;
            playerScale.x *= -1;
            transform.localScale = playerScale;

            Vector3 canvasScale = canvasJugador.transform.localScale;
            canvasScale.x *= -1;
            canvasJugador.transform.localScale = canvasScale;
        }

        private void CheckGround()
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.transform.position, 0.2f);
            isGrounded = colliders.Length > 1;
        }

        private void CheckWall()
        {
            Collider2D[] colliders = Physics2D.OverlapCircleAll(wallCheck.transform.position, 0.2f);
            foreach (Collider2D collider in colliders)
            { 
              if (collider.isTrigger == true) //Aca esta el bug del arbol
              {
                 Debug.Log("No escalable");
                 isTouchingWall = false;
              }
              else if (collider.isTrigger == false)
              {
                 isTouchingWall = colliders.Length > 1;
              }

            }
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Arbol"))
            {
                Arbol arbolGolpeado = other.gameObject.GetComponent<Arbol>();
                if (arbolMasCercano == null ||
                    Vector2.Distance(transform.position, arbolGolpeado.transform.position) < Vector2.Distance(transform.position, arbolMasCercano.transform.position))
                {
                    arbolMasCercano = arbolGolpeado;
                }
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Arbol"))
            {
                Arbol arbolGolpeado = other.gameObject.GetComponent<Arbol>();
                if (arbolMasCercano == arbolGolpeado)
                {
                    arbolMasCercano = null;
                }
            }
        }

        private bool IsPlaying(Animator animator, string stateName)
        {
            return animator.GetCurrentAnimatorStateInfo(0).IsName(stateName);
        }
    }
}
