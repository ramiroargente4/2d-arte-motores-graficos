using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorInteraccionCarro : MonoBehaviour
{
    public UIManager uiManager;
    public GameManager gameManager;
    public BoxCollider2D collider2D;
    public GameObject avisoUI;
    public bool sobreZonaEntrega;
    public ZonaEntrega zonaEntrega;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player") && gameManager.cantidadTroncosEnUsuario >= 1)
        {
            sobreZonaEntrega = true;
            avisoUI.SetActive(true);
            Debug.Log("Aparece E");
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sobreZonaEntrega = false;
            avisoUI.SetActive(false);
            Debug.Log("No mas E");
        }
    }
    void Start()
    {
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        avisoUI = transform.Find("CarroCanvas/AyudaUI").gameObject;
        zonaEntrega = this.GetComponent<ZonaEntrega>();

        avisoUI.SetActive(false);
    }
    private void Update()
    {
        if (sobreZonaEntrega && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Presionaste E dentro de la zona.");
            zonaEntrega.EntregarTroncos();
        }
    }
}
