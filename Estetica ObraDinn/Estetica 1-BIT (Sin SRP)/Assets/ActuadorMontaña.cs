using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActuadorMontaña : MonoBehaviour
{
    // Start is called before the first frame update
    public UIManager uiManager;
    public GameManager gameManager;
    public int idActuador;
    public Animator animatorCierre;

    public static bool accionado1 = false;
    public static bool accionado2 = false;
    public static bool accionado3 = false;

    private void Start()
    {
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        animatorCierre = GameObject.Find("AnimacionAfuera").GetComponent<Animator>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        //Tambien puedo usar funcion corutina si quiero añadir mas textos
        if (other.gameObject.CompareTag("Player") && idActuador == 1)
        {
            uiManager.MostrarTextoGeneral("No se si deberia escalar, podria seguir trabajando"); //El usuario es el que decide 
        }
        if (other.gameObject.CompareTag("Player") && idActuador == 2 && accionado2 == false)
        {
            uiManager.MostrarTextoGeneral("No estoy seguro de esto, todavia podria volver");
            //GestorDeAudio.instancia.FadeOut("Flauta", 4f);
            //GestorDeAudio.instancia.FadeOut("FlautaDos", 6f);
            //GestorDeAudio.instancia.FadeOut("Principal", 10f);
            AkSoundEngine.PostEvent("DesactivarTodoMenosPianoAgudo", gameObject);           
            accionado2 = true;
            gameManager.LlamarFinal();
        }
        //Condiciones dependiendo de que hizo antes
        if (other.gameObject.CompareTag("Player") && idActuador == 3 && accionado3 == false)
        {
            AkSoundEngine.PostEvent("DesactivarPianoAgudo", gameObject);
            //GestorDeAudio.instancia.FadeOut("PianoAgudo", 3f);
            Invoke("ReproducirSonidoFinal", 3f);
            accionado3 = true;
            Invoke("PlayAnimacionFinal", 5f);
        }
    } 
    private void ReproducirSonidoFinal()
    {
        //GestorDeAudio.instancia.ReproducirSonido("SonidoFinal");
        AkSoundEngine.PostEvent("SonidoFinal", gameObject);
    }

    public void PlayAnimacionFinal()
    {
        animatorCierre.Play("AnimacionAfuera");
        Invoke("ReproducirPrincipalDevuelta", 2f);
    }

    private void ReproducirPrincipalDevuelta()
    {
        AkSoundEngine.PostEvent("ReiniciarMusica", gameObject);
        //GestorDeAudio.instancia.FadeIn("Principal", 5f);
    }
}

        
