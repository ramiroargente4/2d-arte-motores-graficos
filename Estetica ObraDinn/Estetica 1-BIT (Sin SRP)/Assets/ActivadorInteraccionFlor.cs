using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorInteraccionFlor : MonoBehaviour
{
    public UIManager uiManager;
    public BoxCollider2D collider2D;
    public int id;
    public GameObject avisoUI;
    public bool sobreZonaFlor;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sobreZonaFlor = true;
            avisoUI.SetActive(true);
            Debug.Log("Aparece E");
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sobreZonaFlor = false;
            avisoUI.SetActive(false);
            Debug.Log("No mas E");
        }
    }
    void Start()
    {
        avisoUI.SetActive(false);
    }
    private void Update()
    {
        if (sobreZonaFlor && Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Presionaste E dentro de la zona.");
            avisoUI.SetActive(false);
            uiManager.InteraccionFlor(this);
        }
    }
}

