using System;
using System.Collections;
using UnityEngine;

public class GestorDeAudio : MonoBehaviour
{
    public Sonido[] sonidos;
    public static GestorDeAudio instancia;

    private Sonido[] pistasMusicales;  
    private AudioSource audioPrincipal;  

    void Awake()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        foreach (Sonido s in sonidos)
        {
            s.FuenteAudio = gameObject.AddComponent<AudioSource>();
            s.FuenteAudio.clip = s.ClipSonido;
            s.FuenteAudio.volume = s.Volumen;
            s.FuenteAudio.pitch = s.pitch;
            s.FuenteAudio.loop = s.repetir;
            s.FuenteAudio.time = s.tiempo;
        }

        Sonido principal = Array.Find(sonidos, sonido => sonido.Nombre == "Principal");
        if (principal != null)
        {
            audioPrincipal = principal.FuenteAudio;
        }
        else
        {
            Debug.LogWarning("Pista musical 'Principal' no encontrada.");
        }
        pistasMusicales = new Sonido[3];  // Asumiendo que habr� 3 pistas musicales espec�ficas
        pistasMusicales[0] = Array.Find(sonidos, sonido => sonido.Nombre == "PianoAgudo");
        pistasMusicales[1] = Array.Find(sonidos, sonido => sonido.Nombre == "Flauta");
        pistasMusicales[2] = Array.Find(sonidos, sonido => sonido.Nombre == "FlautaDos");
    }

    void Update()
    {
        // Sincronizar todas las pistas musicales con el tiempo del audio maestro
        foreach (Sonido p in pistasMusicales)
        {
            if (p != null)
            {
                p.FuenteAudio.time = audioPrincipal.time;
            }
        }
    }

    public void ReproducirSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sound => sound.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Play();
        }
    }

    public void PausarSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.FuenteAudio.Pause();
        }
    }

    public void LoopearSonido(bool activar, string nombre)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (activar == true)
        {
            s.FuenteAudio.loop = true;
        }
        if (activar == false)
        {
            s.FuenteAudio.loop = false;
        }
    }

    public void ReiniciarSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        else
        {
            s.tiempo = 0f;
            s.TiempoActual = 0f;
            s.FuenteAudio.time = s.tiempo;
        }
    }

    public void FadeOut(string nombre, float duracion)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        StartCoroutine(FadeOutCoroutine(s.FuenteAudio, duracion));
    }

    private IEnumerator FadeOutCoroutine(AudioSource audioSource, float duracion)
    {
        float startVolume = audioSource.volume;
        float time = 0;

        while (time < duracion)
        {
            audioSource.volume = Mathf.Lerp(startVolume, 0, time / duracion);
            time += Time.deltaTime;
            yield return null;
        }

        audioSource.volume = 0;
        audioSource.Pause();
        audioSource.volume = startVolume; // Restablece el volumen para la pr�xima reproducci�n
    }

    public void FadeIn(string nombre, float duracion)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (s == null)
        {
            Debug.LogWarning("Sonido: " + nombre + " no encontrado.");
            return;
        }
        s.FuenteAudio.volume = 0;
        s.FuenteAudio.Play();
        StartCoroutine(FadeInCoroutine(s.FuenteAudio, duracion));
    }

    private IEnumerator FadeInCoroutine(AudioSource audioSource, float duracion)
    {
        float startVolume = 0.2f;

        while (audioSource.volume < startVolume)
        {
            audioSource.volume += startVolume * Time.deltaTime / duracion;
            yield return null;
        }

        audioSource.volume = startVolume;
    }
    public void AjustarVolumen(string nombre, float volumen)
    {
        Sonido p = Array.Find(sonidos, sonido => sonido.Nombre == nombre);
        if (p != null)
        {
            p.FuenteAudio.volume = volumen;
        }
        else
        {
            Debug.LogWarning("Pista musical: " + nombre + " no encontrada.");
        }
    }
}
