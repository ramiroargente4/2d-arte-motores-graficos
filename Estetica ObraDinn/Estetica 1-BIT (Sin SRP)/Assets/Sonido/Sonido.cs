using UnityEngine;

[System.Serializable]
public class Sonido
{
    public string Nombre;
    public AudioClip ClipSonido;

    [Range(0f, 1f)]
    public float Volumen;

    [Range(0f, 2f)]
    public float pitch;

    public bool repetir;

    public float tiempo;

    [HideInInspector]
    public AudioSource FuenteAudio;
    public float TiempoActual { get; set; }
}
