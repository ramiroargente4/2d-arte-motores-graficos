/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID ACTIVARLASFLAUTAS = 3467588503U;
        static const AkUniqueID ACTIVARPIANOAGUDO = 1593725560U;
        static const AkUniqueID AGARRANDOMADERA = 1037303700U;
        static const AkUniqueID ARBOLCAYENDOSE = 544432794U;
        static const AkUniqueID ARRANCARMUSICA = 2419255773U;
        static const AkUniqueID CAMINAR = 621083434U;
        static const AkUniqueID DESACTIVARPIANOAGUDO = 4149781682U;
        static const AkUniqueID DESACTIVARTODOMENOSPIANOAGUDO = 1741987938U;
        static const AkUniqueID ESCALADA = 3799266093U;
        static const AkUniqueID ESCRIBIR = 1116358746U;
        static const AkUniqueID HACHAZO = 2578461967U;
        static const AkUniqueID HACHAZOARBOL = 4138102433U;
        static const AkUniqueID PARARCAMINAR = 973926000U;
        static const AkUniqueID PARARESCALADA = 2036532419U;
        static const AkUniqueID PARARESCRIBIR = 3189757836U;
        static const AkUniqueID PRIMERSONIDOPUENTE = 2847390347U;
        static const AkUniqueID REINICIARMUSICA = 1001568911U;
        static const AkUniqueID SEGUNDOSONIDOPUENTE = 3284183669U;
        static const AkUniqueID SOLTANDOMADERA = 3781404233U;
        static const AkUniqueID SONIDOFINAL = 1648373725U;
    } // namespace EVENTS

    namespace STATES
    {
        namespace AMBIENTE
        {
            static const AkUniqueID GROUP = 4095160060U;

            namespace STATE
            {
                static const AkUniqueID BOSQUE = 1641284458U;
                static const AkUniqueID CUEVA = 2274601469U;
                static const AkUniqueID NONE = 748895195U;
            } // namespace STATE
        } // namespace AMBIENTE

    } // namespace STATES

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
