using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivacionAve : MonoBehaviour
{
    public Animator animatorPadre;
    public Animator animatorMovimiento;
    public Animator animatorAve;
    // Start is called before the first frame update
    void Start()
    {
        animatorAve.enabled = false;
        animatorMovimiento.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (animatorPadre != null)
            {
                animatorPadre.enabled = false;
            }
            animatorAve.enabled = true;
            animatorMovimiento.enabled = true;
        }
    }
}
