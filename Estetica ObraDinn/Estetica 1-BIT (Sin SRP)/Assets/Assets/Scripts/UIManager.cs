using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;
using Platformer;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [Header("Referencias")]
    public GameManager gameManager;
    public GameObject player;
    public Camera mainCamera;
    public GameObject objetoCamara;
    public GameObject camaraMenu;
    public Animator animatorMenu;
    public TextMeshProUGUI txtFinalesConseguidos;
    public TextMeshProUGUI textoInteraccion;
    public RawImage cajaInteraccion;
    public Button botonSi;
    public Button botonNo;

    public Animator animatorCierre;

    [Header("Troncos Visuales")]
    public GameObject tronco1;
    public GameObject tronco2;

    [Header("Botones y Flores")]
    public List<Button> botones = new List<Button>();
    public List<ActivadorInteraccionFlor> flores = new List<ActivadorInteraccionFlor>();
    public List<bool> seleccionSi = new List<bool>();

    [Header("Configuraciones de Cámara")]
    public float tamañoCamaraClickeado = 3f;
    private float tamañoOriginalCamara;

    private int florIndex = -1;
    private int index;
    public bool floresCompletadas = false;

    private Coroutine typewriterCoroutine;
    private Coroutine locucionCoroutine;
    private Animator playerAnimator;
    private PlayerController controladorPlayer;

    private bool coroutineActive = false;
    public bool InInteraction = false;
    public bool locucionActive = false;
    public bool locucionPaused = false;

    void Start()
    {
        AkSoundEngine.PostEvent("ArrancarMusica", gameObject);

        tamañoOriginalCamara = mainCamera.orthographicSize;
        playerAnimator = player.GetComponent<Animator>();
        controladorPlayer = player.GetComponent<PlayerController>();

        animatorCierre = GameObject.Find("AnimacionAfuera").GetComponent<Animator>();

        InicializarFlores();
        InicializarUI();
        SuscribirEventosFlores();
    }

    void Update()
    {
        ChequearCompleto();
        ChequearFinales();
    }

    public void InicializarFlores()
    {
        for (int i = 0; i < botones.Count; i++)
        {
            seleccionSi.Add(false);
        }
    }

    private void InicializarUI()
    {
        tronco1.SetActive(false);
        tronco2.SetActive(false);

        textoInteraccion.gameObject.SetActive(false);
        cajaInteraccion.gameObject.SetActive(false);
        botonSi.gameObject.SetActive(false);
        botonNo.gameObject.SetActive(false);

        botonSi.onClick.AddListener(OnBotonSiClick);
        botonNo.onClick.AddListener(OnBotonNoClick);
    }

    public void SuscribirEventosFlores()
    {
        foreach (ActivadorInteraccionFlor flor in flores)
        {
            Debug.Log("Se suscribio al evento de" + flor.id.ToString());
        }
    }

    public void IniciarDeMenu()
    {
        animatorMenu.SetBool("quiereJugar", true);
        Invoke("CambiarDeCamara", 2f);
        locucionCoroutine = StartCoroutine(MostrarTextoIntro());
    }

    private IEnumerator MostrarTextoIntro()
    {
        locucionActive = true;
        yield return new WaitForSeconds(2f);
        MostrarTextoGeneral("Another day, another tree...");
        yield return new WaitForSeconds(5f);
        MostrarTextoGeneral("What else is there to see?");
        yield return new WaitForSeconds(4f);
        MostrarTextoGeneral("Things have been so damn bleak lately");
        yield return new WaitForSeconds(5f);
        MostrarTextoGeneral("Fuck it, I’m thinking too much about this.");
        yield return new WaitForSeconds(5f);
        MostrarTextoGeneral("Just gotta get it over with today and lay back", true);
        yield return new WaitForSeconds(4f);
        MostrarTextoGeneral("I’ll cut down these trees around for the logs", true);
        yield return new WaitForSeconds(3f);
        MostrarTextoGeneral("4 of them will be enough to fill the pushcart", true);
        locucionActive = false;
    }

    public void VolverAMenu()
    {
        animatorMenu.SetBool("quiereJugar", false);
        camaraMenu.transform.position = new Vector3(-76.55001f, 20f);
        camaraMenu.SetActive(true);
        objetoCamara.SetActive(false);
        ResetearAnimacionFinal();
        Invoke("ReiniciarEscena", 0.5f);
    }

    public void ReiniciarEscena()
    {
        SceneManager.LoadScene(0);
    }

    public void ResetearAnimacionFinal()
    {
        animatorCierre.Play("AnimacionAfuera", -1, 0f);
        animatorCierre.Update(0);
        animatorCierre.speed = 0;
    }

    public void CambiarDeCamara()
    {
        gameManager.ReiniciarJuego();
        camaraMenu.SetActive(false);
        objetoCamara.SetActive(true);
    }

    private void ChequearFinales()
    {
        txtFinalesConseguidos.text = $"{gameManager.finalesAlcanzados.Count}/3 finales";
    }

    public void InteraccionFlor(ActivadorInteraccionFlor flor)
    {
        if (locucionActive)
        {
            PauseLocucion();
        }
        index = flores.IndexOf(flor);
        FuncionApretarBoton();
        Invoke("FuncionApretarBoton", 0.5f);
    }


    private void FuncionApretarBoton() //Solucion a que desaparezca en primer click el texto demasiado rapido
    {
        if (index != -1 && InInteraction == false)
        {
            InInteraction = true;
            florIndex = index;
            cajaInteraccion.gameObject.SetActive(true);
            textoInteraccion.gameObject.SetActive(true);
            locucionPaused = true;

            if (seleccionSi[florIndex])
            {
                MostrarTexto("Wow, that's beatiful...");
                FreezePlayer();
                StartCoroutine(EsperarYRestaurarCamara());
            }
            else
            {
                MostrarTexto("Should I stop? I'm working");
                FreezePlayer();
                MostrarBotonesInteraccion(true);
            }
            StartCoroutine(LerpCameraSize(tamañoCamaraClickeado, 0.5f));
        }
    }

    private void OnBotonSiClick()
    {
        seleccionSi[florIndex] = true;
        StartCoroutine(ProseguirDialogo(true));
        FreezePlayer();
    }

    private void OnBotonNoClick()
    {
        StartCoroutine(ProseguirDialogo(false));
        UnfreezePlayer();
    }

    private IEnumerator ProseguirDialogo(bool esSi)
    {
        MostrarBotonesInteraccion(false);

        if (esSi)
        {
            MostrarTexto("Wow...");
            yield return new WaitForSeconds(2.5f);
        }
        OcultarTexto();

        UnfreezePlayer();
        StartCoroutine(LerpCameraSize(tamañoOriginalCamara, 0.5f));

        if (locucionPaused)
        {
            ResumeLocucion();
        }
    }

    private IEnumerator EsperarYRestaurarCamara()
    {
        yield return new WaitForSeconds(2.5f);
        StartCoroutine(LerpCameraSize(tamañoOriginalCamara, 0.5f));
        OcultarTexto();
        UnfreezePlayer();

        if (locucionPaused)
        {
            ResumeLocucion();
        }
    }

    private void FreezePlayer()
    {
        //playerAnimator.Play("Atacando", -1, 0f); // Reiniciar la animación
        playerAnimator.SetInteger("playerState", 0);
        //controladorPlayer.bloqueado = true;
        Invoke("FreezearDespuesDeTiempo", 0.2f);
    }
    private void FreezearDespuesDeTiempo()
    {
        playerAnimator.enabled = false;
        controladorPlayer.movingSpeed = 0;
        controladorPlayer.jumpForce = 0;
        controladorPlayer.bloqueado = true;
    }

    private void UnfreezePlayer()
    {
        playerAnimator.enabled = true;
        controladorPlayer.movingSpeed = gameManager.velocidadMovimientoNativa;
        controladorPlayer.jumpForce = gameManager.velocidadSaltoNativa;
        gameManager.ActualizarCapacidades();
        controladorPlayer.bloqueado = false;
    }

    public void ActualizarUIDiegetica()
    {
        if (gameManager.cantidadTroncosEnUsuario == 1)
        {
            tronco1.SetActive(true);
            tronco2.SetActive(false);
        }
        if (gameManager.cantidadTroncosEnUsuario == 2)
        {
            tronco1.SetActive(true);
            tronco2.SetActive(true);
        }
        if (gameManager.cantidadTroncosEnUsuario == 0)
        {
            tronco1.SetActive(false);
            tronco2.SetActive(false);
        }
    }

    private void ChequearCompleto()
    {
        floresCompletadas = seleccionSi.TrueForAll(seleccion => seleccion);
    }

    public void MostrarTextoGeneral(string texto, bool desactivarAutomatico = false) //Recordar ponerlo en true si quiero que se desactive un dialogo despues de que se escriba
    {
        if (!coroutineActive)
        {
            AkSoundEngine.PostEvent("Escribir", gameObject);
            coroutineActive = true;
            MostrarCajasInteraccion(true);
            StartCoroutine(MostrarTextoGeneralCoroutine(texto, desactivarAutomatico));
        }
    }
    private IEnumerator MostrarTextoGeneralCoroutine(string texto, bool desactivarAutomatico)
    {
        MostrarTexto(texto);
        yield return new WaitForSeconds(2.5f + texto.Length * 0.05f);
        if (desactivarAutomatico == true) //Para que no se desactive cuando esta esperando a decidir
        {
            OcultarTexto(); 
        }
        coroutineActive = false;
    }

    private void MostrarTexto(string texto)
    {
        if (typewriterCoroutine != null)
        {
            StopCoroutine(typewriterCoroutine);
        }
        typewriterCoroutine = StartCoroutine(TypeWriterEffect(texto));
    }

    private void OcultarTexto()
    {
        textoInteraccion.gameObject.SetActive(false);
        cajaInteraccion.gameObject.SetActive(false);
        textoInteraccion.text = "";
    }

    private IEnumerator TypeWriterEffect(string text)
    {
        textoInteraccion.text = "";
        foreach (char c in text)
        {
            textoInteraccion.text += c;
            yield return new WaitForSeconds(0.05f);
        }
        if (textoInteraccion.text == text)
        {
            Debug.Log("Termino");
            AkSoundEngine.PostEvent("PararEscribir", gameObject);
        }
    }

    private IEnumerator LerpCameraSize(float tamaño, float duracion)
    {
        float tiempoPasado = 0f;
        float tamañoInicial = mainCamera.orthographicSize;

        while (tiempoPasado < duracion)
        {
            tiempoPasado += Time.deltaTime;
            mainCamera.orthographicSize = Mathf.Lerp(tamañoInicial, tamaño, tiempoPasado / duracion);
            yield return null;
        }
    }

    private void MostrarBotonesInteraccion(bool mostrar)
    {
        botonSi.gameObject.SetActive(mostrar);
        botonNo.gameObject.SetActive(mostrar);
    }

    private void MostrarCajasInteraccion(bool mostrar)
    {
        textoInteraccion.gameObject.SetActive(mostrar);
        cajaInteraccion.gameObject.SetActive(mostrar);
    }

    private void PauseLocucion()
    {
        if (locucionCoroutine != null)
        {
            StopCoroutine(locucionCoroutine);
            locucionPaused = true;
        }
    }

    private void ResumeLocucion()
    {
        if (locucionPaused)
        {
            InInteraction = false;
            locucionCoroutine = StartCoroutine(MostrarTextoIntro());
            locucionPaused = false;
        }
    }
}
