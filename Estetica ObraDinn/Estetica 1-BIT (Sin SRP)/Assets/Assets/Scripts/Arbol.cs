using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arbol : MonoBehaviour
{
    private GameManager gameManager;
    private UIManager uiManager;

    public GameObject tronquitoIcono;
    public GameObject nubeFondo;
    public GameObject nubeYClick;

    public int vecesGolpeado;
    public bool arbolCaido;
    public int maderaContenida;
    public int maderaContenidaNativa;
    public Vector3 posicionOriginal;
    public Quaternion rotacionOriginal;
    public Animator animatorArbol;
    public Collider2D collidersas;
    private bool animatorEnabled = false; // Controla si el Animator est� activo

    void Start()
    {
        posicionOriginal = transform.position;
        rotacionOriginal = transform.rotation;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        nubeFondo = transform.Find("Canvas/NubeYClick/NubeTroncos").gameObject;
        nubeYClick = transform.Find("Canvas/NubeYClick").gameObject;

        nubeYClick.SetActive(false);

        ActualizarCanvasDeArbol();
        collidersas = GetComponent<Collider2D>();
        animatorArbol = GetComponent<Animator>();
        animatorArbol.enabled = false;
        animatorEnabled = false;

        maderaContenidaNativa = maderaContenida;
    }

    public void Reiniciarse() 
    {
       animatorArbol.enabled = false;
       animatorEnabled = false;
       animatorArbol.Play("AnimacionAfuera", -1, 0f); // Reiniciar la animaci�n
       animatorArbol.Update(0); // Actualizar el estado de la animaci�n inmediatamente
       animatorArbol.speed = 0; // Pausar la animaci�n
       maderaContenida = maderaContenidaNativa;
       arbolCaido = false;
       vecesGolpeado = 0;
       transform.position = posicionOriginal;
       transform.rotation = rotacionOriginal;
       DesactivarCanvasArbol(); 
       
      //Habria que guardar la posicion inicial del arbol, todo su componenente transform e igualarlo ahi
    }
    public void ChequeoGolpes()
    {
        vecesGolpeado += 1;
        //collidersas.isTrigger = false; //Hay que corregirlo para que no necesites moverte para cortarlo

        if (vecesGolpeado <= 3)
        {
            //GestorDeAudio.instancia.ReproducirSonido("Hachazo");
            AkSoundEngine.PostEvent("HachazoArbol", gameObject);
            if (animatorEnabled)
            {
                animatorArbol.enabled = false;
                animatorEnabled = false;
            }
            float currentZ = transform.rotation.eulerAngles.z;
            float newRotationZ = currentZ - 5f;
            Debug.Log($"Rotando el �rbol de {currentZ} grados a {newRotationZ} grados");

            // Aplicar rotaci�n usando Quaternion.Euler
            transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, newRotationZ);

            // Asegurarse de que el Animator est� desactivado
        }
        else if (vecesGolpeado == 4)
        {
            // Activar el Animator y reproducir la animaci�n
            if (!animatorEnabled)
            {
                animatorArbol.enabled = true;
                animatorArbol.speed = 1;
                animatorEnabled = true;
                //GestorDeAudio.instancia.ReproducirSonido("ArbolCayendose");
                AkSoundEngine.PostEvent("ArbolCayendose", gameObject);
            }
            animatorArbol.Play("ArbolCae");
            arbolCaido = true;
            Invoke("ActivarCanvasArbol", 2.5f); //Esperamos a que se cae y de ahi activamos las indicaciones
        }
    }
    void ActivarCanvasArbol()
    {
        nubeYClick.SetActive(true);
    }
    void DesactivarCanvasArbol()
    {
        nubeYClick.SetActive(false);
    }
    public void ActualizarCanvasDeArbol() //Iconos
    {
        Transform parentTransform = nubeFondo.transform;

        // Elimina todos los hijos actuales del parentTransform
        foreach (Transform child in parentTransform)
        {
            Destroy(child.gameObject);
        }

        // Instancia nuevos iconos seg�n el valor de maderaContenida
        for (int i = 0; i < maderaContenida; i++)
        {
            GameObject nuevoTronquitoIcono = Instantiate(tronquitoIcono);
            nuevoTronquitoIcono.transform.SetParent(parentTransform, false);
        }
    }
    public void SumarTroncosDeArbol()
    {
        if (arbolCaido == true && maderaContenida > 0)
        {
            if (gameManager.cantidadTroncosEnUsuario < gameManager.capacidadUsuario)
            {
                AkSoundEngine.PostEvent("AgarrandoMadera", gameObject);
                //GestorDeAudio.instancia.ReproducirSonido("AgarrandoMadera");
                maderaContenida--;
                gameManager.cantidadTroncosEnUsuario++;
                gameManager.ActualizarCapacidades();
                ActualizarCanvasDeArbol();
            }
        }
        if (maderaContenida == 0)
        {
            gameObject.SetActive(false);
            //Desaparecer arbol
        }
        if (gameManager.cantidadTroncosEnUsuario == gameManager.capacidadUsuario)
        {
            uiManager.MostrarTextoGeneral("I can't carry so much...", true);
            Debug.Log("No puedo cargar mas");
        }
        ActualizarCanvasDeArbol();
    }
    /*else if (arbolCaido == false)
    {
    //uiManager.MostrarTextoGeneral("Todavia tengo que tirar el arbol");
    Debug.Log("Todavia tengo que tirar el arbol");
    }*/
}
