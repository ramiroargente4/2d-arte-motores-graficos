using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZonaEntrega : MonoBehaviour
{
    public GameObject layoutTroncos;
    public GameObject tronquitoIcono;
    public GameObject noHayTronquitoIcono;
    public GameManager gameManager;
    public UIManager uiManager;

    public int troncosEntregadosEnEsteSpot;
    public int troncosAEntregarEnSpot;
    public int troncosAEntregarEnSpotNativo;


    public int idZonaEntrega;

    [SerializeField] public static bool entrega1Completada;
    [SerializeField] public static bool entrega2Completada;

    private void Start()
    {
        layoutTroncos = transform.Find("CarroCanvas/Layout").gameObject;
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        troncosAEntregarEnSpotNativo = troncosAEntregarEnSpot;
        ManejarEntregas();
    }

    public void Reiniciarse()
    {
        troncosAEntregarEnSpot = troncosAEntregarEnSpotNativo;
        entrega1Completada = false;
        entrega2Completada = false;
    }
    public void EntregarTroncos()
    {
        if (gameManager.cantidadTroncosEnUsuario > 0 && troncosEntregadosEnEsteSpot < troncosAEntregarEnSpot)
        {
            gameManager.EntregarTroncos();
            troncosEntregadosEnEsteSpot++;
            //GestorDeAudio.instancia.ReproducirSonido("SoltandoMadera");
            AkSoundEngine.PostEvent("SoltandoMadera", gameObject);
            ManejarEntregas();
        }
        else if (troncosEntregadosEnEsteSpot == troncosAEntregarEnSpot)
        {
            uiManager.MostrarTextoGeneral("I've brought all the logs needed.", true);
        }
        else if (gameManager.cantidadTroncosEnUsuario == 0)
        {
            uiManager.MostrarTextoGeneral("I don't have any logs.", true);
        }
        if (troncosAEntregarEnSpot == troncosEntregadosEnEsteSpot)
        {
            if (idZonaEntrega == 1)
            {
                entrega1Completada = true;
            }
            if (idZonaEntrega == 2)
            {
                entrega2Completada = true;
            }
        }

    }
    private void ManejarEntregas()
    {
        Transform parentTransform = layoutTroncos.transform;

        // Elimina todos los hijos actuales del parentTransform
        foreach (Transform child in parentTransform)
        {
            Destroy(child.gameObject);
        }
        // Instancia nuevos iconos seg�n el valor de maderaContenida
        for (int i = 0; i < troncosEntregadosEnEsteSpot; i++)
        {
            GameObject nuevoTronquitoIcono = Instantiate(tronquitoIcono);
            nuevoTronquitoIcono.transform.SetParent(parentTransform, false);
        }
        for (int i = 0; i < troncosAEntregarEnSpot - troncosEntregadosEnEsteSpot; i++)
        {
            GameObject nuevoNoHayTronquitoIcono = Instantiate(noHayTronquitoIcono);
            nuevoNoHayTronquitoIcono.transform.SetParent(parentTransform, false);
        }
    }
}
