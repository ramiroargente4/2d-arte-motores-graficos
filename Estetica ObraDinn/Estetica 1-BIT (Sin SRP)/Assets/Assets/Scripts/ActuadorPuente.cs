using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActuadorPuente : MonoBehaviour
{
    public GameObject primeraParteTrabajo;
    public GameObject segundaParteFelicidad;
    private UIManager uiManager;
    public ActuadoresVisuales actuadorFlor;

    public GameObject entregaLlenarPuente;
    public List<GameObject> piezasDelPuente;
    public ZonaEntrega zonaEntregaPuente;

    private bool activoP1 = false;
    private bool activoP2 = false;
    private bool activoP3 = false;
    public static bool pasoPrimeraZona;
    public static bool pianoActivo = false;
    private int conteoConstruccionPuente = 0;

    public bool entroPrimerDialogo = false;
    // Start is called before the first frame update
    void Start()
    {
        //primeraParteTrabajo = transform.Find("PlataformaPrimera/PrimeraParte(Trabajo)").gameObject;
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();

        //zonaEntregaPuente = entregaLlenarPuente.GetComponent<ZonaEntrega>();
        primeraParteTrabajo.SetActive(true);
        //segundaParteFelicidad.SetActive(false);


        entregaLlenarPuente.SetActive(false);
        // Desactivamos todas las piezas primero
        foreach (var pieza in piezasDelPuente)
        {
            pieza.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Activamos la cantidad de piezas igual a los troncos entregados
        for (int i = 0; i < zonaEntregaPuente.troncosEntregadosEnEsteSpot && i < piezasDelPuente.Count; i++)
        {
            if (i < 5)
            {
                piezasDelPuente[i].SetActive(true);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (pianoActivo == false)
            {
                //GestorDeAudio.instancia.FadeIn("PianoAgudo", 5f);
                AkSoundEngine.PostEvent("ActivarPianoAgudo", gameObject);
                pianoActivo = true;
            }
            if (entroPrimerDialogo == false && actuadorFlor.florAutoctona == true)
            {
                Debug.Log("LocucionPuente");
                StartCoroutine(Puente());
                entroPrimerDialogo = true;
            }
        }
    }
    public IEnumerator Puente()
    {
        uiManager.locucionActive = true;
        yield return new WaitForSeconds(2f);
        uiManager.MostrarTextoGeneral("This bridge has been broken for ages");
        yield return new WaitForSeconds(5f);
        uiManager.MostrarTextoGeneral("I wonder what�s on the other side");
        yield return new WaitForSeconds(7f);
        entregaLlenarPuente.SetActive(true);
    }

        private void SonidosPuente()
    {
        if (conteoConstruccionPuente == 1)
        {
            //GestorDeAudio.instancia.ReproducirSonido("PrimerSonidoPuente");
            AkSoundEngine.PostEvent("PrimerSonidoPuente", gameObject);
        }
        if (conteoConstruccionPuente == 2)
        {
            //GestorDeAudio.instancia.ReproducirSonido("SegundoSonidoPuente");
            AkSoundEngine.PostEvent("SegundoSonidoPuente", gameObject);
        }
    }
}