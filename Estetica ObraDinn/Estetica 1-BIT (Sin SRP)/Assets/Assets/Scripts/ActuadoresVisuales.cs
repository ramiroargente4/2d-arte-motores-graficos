using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActuadoresVisuales : MonoBehaviour
{
    public bool bosqueClaro;
    public bool florAutoctona;
    public bool bosqueOscuro;
    public int idActuadorVisual;
    public static int visualActual;
    public Material zonaBosqueMat;
    public ObraDinn obraDinn;
    public UIManager uiManager;
    public static bool inicioFlautas = false;
    private void Start()
    {
        visualActual = 0;
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        //obraDinn = GameObject.Find("Main Camera").GetComponent<ObraDinn>();
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (idActuadorVisual == 0 && florAutoctona == false && uiManager.seleccionSi[0] == true)
            {
                florAutoctona = true;
                StartCoroutine(ReflexionFlor());
            }
            if (idActuadorVisual == 1 && bosqueClaro == false)
            {
                bosqueClaro = true;
                visualActual = idActuadorVisual;
                StartCoroutine(ReflexionBosqueClaro());
            }
            if (idActuadorVisual == 2 && bosqueOscuro == false)
            {
                bosqueOscuro = true;
                visualActual = idActuadorVisual;
                obraDinn.thresholdMat = zonaBosqueMat;
                //if (inicioFlautas == false)
                //{
                //    //GestorDeAudio.instancia.FadeIn("Flauta", 4f);
                //    //GestorDeAudio.instancia.FadeIn("FlautaDos", 4f);
                //    //GestorDeAudio.instancia.ReproducirSonido("Flauta");
                //    //GestorDeAudio.instancia.ReproducirSonido("FlautaDos");
                //    inicioFlautas = true;
                //}
                AkSoundEngine.SetState("Ambiente", "Bosque");
                AkSoundEngine.PostEvent("ActivarLasFlautas", gameObject);
                StartCoroutine(ReflexionBosqueOscuro());
            }

        }
    
    }
    public IEnumerator ReflexionFlor()
    {
        uiManager.locucionActive = true;
        yield return new WaitForSeconds(2f);
        uiManager.MostrarTextoGeneral("What is this flower doing here�");
        yield return new WaitForSeconds(5f);
        uiManager.MostrarTextoGeneral("Doesn't look like a native one at all");
        yield return new WaitForSeconds(5f);
        uiManager.MostrarTextoGeneral("Nevermind, gotta get back to work");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("No time for thinking about silly stuff", true);
        uiManager.locucionActive = false;
        yield return new WaitForSeconds(2f);

    }

    public IEnumerator ReflexionBosqueClaro()
    {
        uiManager.locucionActive = true;
        yield return new WaitForSeconds(2f);
        uiManager.MostrarTextoGeneral("Wow, I�ve never been on this forest");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("Everything�s so different here");
        yield return new WaitForSeconds(5f);
        uiManager.MostrarTextoGeneral("Flowers, trees, colors, so full of life");
        yield return new WaitForSeconds(3f);
        uiManager.MostrarTextoGeneral("I wonder how could I have missed this", true);
        uiManager.locucionActive = false;
    }

    public IEnumerator ReflexionBosqueOscuro()
    {
        uiManager.locucionActive = true;
        yield return new WaitForSeconds(2f);
        uiManager.MostrarTextoGeneral("Something�s changed, life�s escaped");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("Everything is now so bleak, so rotten");
        yield return new WaitForSeconds(6f);
        uiManager.MostrarTextoGeneral("Why did I even come here?");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("Maybe it was a mistake");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("Maybe I should�ve just seized the chance..,");
        yield return new WaitForSeconds(3f);
        uiManager.MostrarTextoGeneral("Cut down all I could and gone back home");
        yield return new WaitForSeconds(4f);
        uiManager.MostrarTextoGeneral("Stop looking for things that don�t exist");
        yield return new WaitForSeconds(3f);
        uiManager.MostrarTextoGeneral("To just work and sleep, cut and kick?");
        yield return new WaitForSeconds(2f);
        uiManager.MostrarTextoGeneral("I don�t know man�");
        uiManager.locucionActive = false;
    }

}
