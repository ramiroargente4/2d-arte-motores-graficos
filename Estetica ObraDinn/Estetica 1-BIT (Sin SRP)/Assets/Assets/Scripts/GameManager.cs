using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Platformer;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Header("Importaciones")]
    public PlayerController playerController;
    public UIManager uiManager;

    [Header("Manejo troncos")]
    public int cantidadTroncosEnUsuario;
    public int cantidadTroncosEntregados;

    public TextMeshProUGUI txtTroncosCargados;
    public TextMeshProUGUI txtTroncosRestantes;

    [SerializeField] private int troncosAEntregar;
    [SerializeField] public int capacidadUsuario;

    [Header("PowerDowns")]
    [SerializeField] public float velocidadMovimientoNativa;
    [SerializeField] public float velocidadSaltoNativa;
    [SerializeField] public float velocidadEscaladaNativa;
    [SerializeField] private float factorRestaMovimiento = 0.5f;
    [SerializeField] private float factorRestaEscalada = 0.1f;
    [SerializeField] private float factorRestaSalto = 0.25f;

    [Header("Timer")]
    public TextMeshProUGUI txtTiempoRestante;
    public float tiempoInicial;
    [SerializeField] private float tiempoRestante;
    [SerializeField] private bool timerActivo = false;

    [Header("Manejo Finales")]
    public GameObject fuegoFinal;
    public GameObject objetosFinal2;
    public TextMeshProUGUI textoFinal;
    [SerializeField] bool final1;
    [SerializeField] bool final2;
    [SerializeField] bool final3;
    public GameObject canvasHUD;
    public List<bool> finalesAlcanzados = new List<bool>(); // Habria que hacerlo prevalente mas alla de cierre

    [Header("Checkpoints")]
    public List<Vector3> coordenadasCheckpoints = new List<Vector3>();
    public ActuadoresVisuales actuadorBosque;
 
    // Start is called before the first frame update
    void Start()
    {
        //ReiniciarDatosFinales();
        CargarDatosFinales();
        CargarFinalesAlcanzados();

        velocidadMovimientoNativa = playerController.movingSpeed;
        velocidadSaltoNativa = playerController.jumpForce;
        velocidadEscaladaNativa = playerController.wallClimbSpeed;

        AkSoundEngine.SetState("Ambiente", "Cueva");

        fuegoFinal = GameObject.Find("Fuego");
        objetosFinal2 = GameObject.Find("Final2");
        //fuegoFinal.SetActive(false);
        objetosFinal2.SetActive(false);
    }

    public void GuardarDatosFinales()
    {
        // Guardar el estado de los finales en PlayerPrefs
        PlayerPrefs.SetInt("Final1", final1 ? 1 : 0);
        PlayerPrefs.SetInt("Final2", final2 ? 1 : 0);
        PlayerPrefs.SetInt("Final3", final3 ? 1 : 0);
        PlayerPrefs.Save();
    }

    private void CargarDatosFinales()
    {
        final1 = PlayerPrefs.GetInt("Final1", 0) == 1;
        final2 = PlayerPrefs.GetInt("Final2", 0) == 1;
        final3 = PlayerPrefs.GetInt("Final3", 0) == 1;
    }

    private void GuardarFinalesAlcanzados()
    {
        for (int i = 0; i < finalesAlcanzados.Count; i++)
        {
            PlayerPrefs.SetInt("FinalAlcanzado" + i, finalesAlcanzados[i] ? 1 : 0);
        }
        PlayerPrefs.SetInt("FinalesAlcanzadosCount", finalesAlcanzados.Count);
    }

    private void CargarFinalesAlcanzados()
    {
        finalesAlcanzados.Clear();
        int count = PlayerPrefs.GetInt("FinalesAlcanzadosCount", 0);
        for (int i = 0; i < count; i++)
        {
            finalesAlcanzados.Add(PlayerPrefs.GetInt("FinalAlcanzado" + i, 0) == 1);
        }
    }

    public void ReiniciarDatosFinales()
    {
        final1 = false;
        final2 = false;
        final3 = false;

        finalesAlcanzados.Clear();

        PlayerPrefs.SetInt("Final1", 0);
        PlayerPrefs.SetInt("Final2", 0);
        PlayerPrefs.SetInt("Final3", 0);
        PlayerPrefs.SetInt("FinalesAlcanzadosCount", 0);

        // Guardar los cambios inmediatamente
        PlayerPrefs.Save();
    }

    // Update is called once per frame
    void Update()
    {
        ChequearCaida();
    }
    public void ArrancarCronometro()
    {
        tiempoRestante = tiempoInicial;
        IniciarCuentaRegresiva(tiempoInicial);
    }
    public void ChequearCaida()
    {
        if (playerController.transfUsuario.position.y <= -25)
        {
            ReiniciarJuego();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReiniciarJuego();
        }
    }

    /*public void SumarTroncos()
    {
        if (cantidadTroncosEnUsuario < capacidadUsuario)
        {
            cantidadTroncosEnUsuario++;
            ActualizarCapacidades();
        }
        else
        {
            Debug.Log("No podes llevar mas troncos!");
        }

        ActualizarUI();
    }*/
    public void EntregarTroncos()
    {
        if (cantidadTroncosEnUsuario > 0)
        {
                cantidadTroncosEnUsuario--;
                cantidadTroncosEntregados++;        
        }

        ActualizarCapacidades();
        ActualizarUI();
    }
    public void LlamarFinal()
    {
        //FINAL 1
        if (uiManager.floresCompletadas == false && ZonaEntrega.entrega1Completada == true && ZonaEntrega.entrega2Completada == false)
        {
            objetosFinal2.SetActive(false);
            fuegoFinal.SetActive(false);
            textoFinal.text = "Final 1/3";
            if (final1 == false)
            {
                final1 = true;
                finalesAlcanzados.Add(true);
            }
        }
        //FINAL 2
        if (uiManager.floresCompletadas == false && ZonaEntrega.entrega1Completada == true && ZonaEntrega.entrega2Completada == true)
        {
            objetosFinal2.SetActive(true);
            fuegoFinal.SetActive(false);
            textoFinal.text = "Final 2/3";
            if (final2 == false)
            {
                final2 = true;
                finalesAlcanzados.Add(true);
            }
        }
        //FINAL 3 
        if (uiManager.floresCompletadas == true && ZonaEntrega.entrega1Completada == true && ZonaEntrega.entrega2Completada == true)
        {
            //uiManager.MostrarTextoGeneral("Gracias, necesitaba esto");
            objetosFinal2.SetActive(false);
            fuegoFinal.SetActive(true);
            textoFinal.text = "Final 3/3";
            if (final3 == false)
            {
                final3 = true;
                finalesAlcanzados.Add(true);
            }
        }
        GuardarDatosFinales();
        GuardarFinalesAlcanzados();
    }
    public void ReiniciarJuego()
    {
        tiempoRestante = tiempoInicial;
        playerController.movingSpeed = velocidadMovimientoNativa;
        playerController.jumpForce = velocidadSaltoNativa;
        ReinicioACheckpoints();
        //uiManager.objetoCamara.transform.position = new Vector3(-76.61429f, -9.786943f);
        //cantidadTroncosEntregados = 0;
        //cantidadTroncosEnUsuario = 0;
        ActualizarUI();
        //ActualizarCapacidades();
        uiManager.ActualizarUIDiegetica();
        if (timerActivo == false)
        {
            ArrancarCronometro();
        }
        //ReiniciarTodosLosArboles();
        //ReiniciarTodasLasEntregas();
        //uiManager.InicializarFlores();
        //uiManager.SuscribirEventosBotones();
        canvasHUD.SetActive(true); //Solo tiene la anim final
    }

    public void ReinicioACheckpoints()
    {
        playerController.transfUsuario.position = coordenadasCheckpoints[1];

        //if (actuadorBosque.visualActual == 0)
        //{
        //    playerController.transfUsuario.position = coordenadasCheckpoints[1];
        //}
        //if (actuadorBosque.visualActual == 1)
        //{
        //    playerController.transfUsuario.position = coordenadasCheckpoints[0];
        //}
    }

    public void ReiniciarTodosLosArboles()
    {
        Arbol[] arboles = FindObjectsOfType<Arbol>();
        foreach (Arbol arbol in arboles)
        {
            arbol.Reiniciarse();
        }
    }

    public void ReiniciarTodasLasEntregas()
    {
        ZonaEntrega[] zonaEntregas = FindObjectsOfType<ZonaEntrega>();
        foreach (ZonaEntrega zonaEntrega in zonaEntregas)
        {
            zonaEntrega.Reiniciarse();
        }
    }


    public void ActualizarCapacidades()
    {
        if (cantidadTroncosEnUsuario > 0)
        {
            playerController.movingSpeed -= factorRestaMovimiento * cantidadTroncosEnUsuario;
            playerController.jumpForce -= factorRestaSalto * cantidadTroncosEnUsuario;
            playerController.wallClimbSpeed -= factorRestaEscalada * cantidadTroncosEnUsuario;
        }
        if (cantidadTroncosEnUsuario == 0)
        {
            playerController.movingSpeed = velocidadMovimientoNativa;
            playerController.jumpForce = velocidadSaltoNativa;
            playerController.wallClimbSpeed = velocidadEscaladaNativa;
        }
        uiManager.ActualizarUIDiegetica();
    }
    private void ActualizarUI() //Deprecado
    {
        txtTroncosCargados.text = cantidadTroncosEnUsuario.ToString() + "/" + capacidadUsuario.ToString();
        txtTroncosRestantes.text = cantidadTroncosEntregados.ToString() + "/" + troncosAEntregar.ToString();
    }
    void IniciarCuentaRegresiva(float tiempoInicial)
    {
        tiempoRestante = tiempoInicial;
        timerActivo = true;
        StartCoroutine(ContarRegresivamente());
    }

    void DetenerTemporizador()
    {
        timerActivo = false;
    }

    IEnumerator ContarRegresivamente()
    {
        while (timerActivo && tiempoRestante >= 0)
        {
            tiempoRestante -= Time.deltaTime;
            txtTiempoRestante.text = FormatearTiempo(tiempoRestante);
            yield return null;
        }

        if (tiempoRestante <= 0)
        {
            Debug.Log("Se termino");
            timerActivo = false;
            tiempoRestante = 0f;
            txtTiempoRestante.text = FormatearTiempo(tiempoRestante);
        }
    }
    public string FormatearTiempo(float tiempo)
    {
        int minutos = Mathf.FloorToInt(tiempo / 60);
        int segundos = Mathf.FloorToInt(tiempo % 60);

        if (tiempo >= 60)
        {
            return minutos.ToString("00") + ":" + segundos.ToString("00");
        }
        else
        {
            return "00:" + segundos.ToString("00");
        }
    }
}
