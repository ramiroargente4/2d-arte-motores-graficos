using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorInteraccionArbol : MonoBehaviour
{
    public UIManager uiManager;
    public BoxCollider2D collider2D;
    public GameObject avisoUI;
    public bool sobreZonaArbol;
    public bool sobreArbolCaido;
    public Arbol arbol;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sobreZonaArbol = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            sobreZonaArbol = false;
            avisoUI.SetActive(false);
            Debug.Log("No mas E");
        }
    }
    void Start()
    {
        uiManager = GameObject.Find("GameManager").GetComponent<UIManager>();
        avisoUI = transform.Find("Canvas/NubeYClick/AyudaUI").gameObject;
        arbol = this.GetComponent<Arbol>();

        avisoUI.SetActive(false);
    }
    private void Update()
    {
        if (sobreZonaArbol && arbol.arbolCaido == true)
        {
            sobreZonaArbol = true;
            sobreArbolCaido = true;
            avisoUI.SetActive(true);
            Debug.Log("Aparece E");
            if (Input.GetKeyDown(KeyCode.E))
            {
                Debug.Log("Presionaste E dentro de la zona.");
                arbol.SumarTroncosDeArbol();
            }
        }
        else if (sobreZonaArbol && arbol.arbolCaido == false)
        {
            sobreArbolCaido = false;
            avisoUI.SetActive(false);
        }
    }
}
